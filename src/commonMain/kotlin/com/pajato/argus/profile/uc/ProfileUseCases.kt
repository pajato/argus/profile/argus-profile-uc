package com.pajato.argus.profile.uc

import com.pajato.argus.profile.core.ProfileRepo
import com.pajato.argus.profile.core.SelectableProfile
import java.net.URI

public object ProfileUseCases {
    public suspend fun loadProfileData(repo: ProfileRepo, uri: URI) { repo.injectDependency(uri) }

    public fun add(repo: ProfileRepo, item: SelectableProfile): Unit = repo.register(item)

    public fun modify(repo: ProfileRepo, item: SelectableProfile): Unit = repo.register(item)

    public fun get(repo: ProfileRepo, id: Int): SelectableProfile? = repo.cache[id]

    public fun getAll(repo: ProfileRepo): List<SelectableProfile> = repo.cache.values.toList()

    public fun toggleSelection(repo: ProfileRepo, item: SelectableProfile): Unit = repo.toggleSelected(item)
}
