package com.pajato.argus.profile.uc

import com.pajato.argus.profile.core.I18nStrings
import com.pajato.argus.profile.core.ProfileCache
import com.pajato.argus.profile.core.ProfileRepo
import com.pajato.argus.profile.core.ProfileRepoError
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.persister.jsonFormat
import com.pajato.persister.persist
import com.pajato.persister.readAndPruneData
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.URI

object TestProfileRepo : ProfileRepo {
    override val cache: ProfileCache = mutableMapOf()
    private var profileUri: URI? = null

    override suspend fun injectDependency(uri: URI) {
        fun handler(cause: Throwable) { throw cause }
        validateUri(uri, ::handler)
        readAndPruneData(uri, cache, SelectableProfile.serializer(), TestProfileRepo::getKeyFromItem)
        profileUri = uri
    }

    override fun register(json: String) {
        val uri = profileUri ?: throw ProfileRepoError(get(I18nStrings.PROFILE_URI_ERROR))
        val item = jsonFormat.decodeFromString(SelectableProfile.serializer(), json)
        cache[item.profile.id] = item
        runBlocking { launch(IO) { persist(uri, json) } }
    }

    override fun register(item: SelectableProfile) {
        val uri = profileUri ?: throw ProfileRepoError(get(I18nStrings.PROFILE_URI_ERROR))
        val json = jsonFormat.encodeToString(SelectableProfile.serializer(), item)
        cache[item.profile.id] = item
        runBlocking { launch(IO) { persist(uri, json) } }
    }

    override fun toggleHidden(item: SelectableProfile) { TODO("Not yet implemented") }

    override fun toggleSelected(item: SelectableProfile) = register(item.copy(isSelected = !item.isSelected))

    private fun getKeyFromItem(item: SelectableProfile) = item.profile.id
}
