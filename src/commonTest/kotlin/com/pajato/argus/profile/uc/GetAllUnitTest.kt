package com.pajato.argus.profile.uc

import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetAllUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        fun getUrl() = loader.getResource("files/profiles.txt") ?: fail("Could not load profiles resource file!")
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestProfileRepo.injectDependency(getUrl().toURI()) }
    }

    @Test fun `When  an item, verify behavior`() {
        val list = ProfileUseCases.getAll(TestProfileRepo)
        assertEquals(9, list.size, "$list")
    }
}
