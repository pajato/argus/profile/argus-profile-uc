package com.pajato.argus.profile.uc

import com.pajato.argus.profile.core.Profile
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.argus.profile.uc.ProfileUseCases.add
import com.pajato.argus.profile.uc.ProfileUseCases.get
import com.pajato.argus.profile.uc.ProfileUseCases.toggleSelection
import com.pajato.argus.profile.uc.TestProfileRepo.cache
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class ToggleSelectionUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val url = loader.getResource("files/profiles.txt") ?: fail("Could not load profiles resource file!")
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestProfileRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When an added item is toggled item, verify behavior`() {
        val item = SelectableProfile(false, false, Profile(0))
        cache.clear()
        add(TestProfileRepo, item)
        assertEquals(1, cache.size)
        assertToggledItem(true)
        assertToggledItem(false)
    }

    private fun assertToggledItem(value: Boolean) {
        fun getItem(): SelectableProfile = get(TestProfileRepo, 0) ?: fail("Item should not be null!")
        toggleSelection(TestProfileRepo, getItem())
        assertEquals(value, getItem().isSelected)
    }
}
