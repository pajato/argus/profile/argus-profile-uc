package com.pajato.argus.profile.uc

import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class LoadProfileDataUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() { copyResourceDirs(loader, "read-only-files", "files") }

    @Test fun `When loading the movie file, verify movie repo size`() {
        val loader = this::class.java.classLoader
        val path = "files/profiles.txt"
        val uri = loader.getResource(path)?.toURI() ?: fail(ERROR_MSG)
        runBlocking {
            ProfileUseCases.loadProfileData(TestProfileRepo, uri)
            assertEquals(9, TestProfileRepo.cache.values.size)
        }
    }

    companion object {
        const val ERROR_MSG = "Could not load the resource test data!"
    }
}
