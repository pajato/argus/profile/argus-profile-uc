package com.pajato.argus.profile.uc

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class ProfileUseCasesUnitTest : ReportingTestProfiler() {
    @Test fun foo() { assertEquals(null, ProfileUseCases.get(TestProfileRepo, Int.MAX_VALUE)) }
}
