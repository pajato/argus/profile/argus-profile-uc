package com.pajato.argus.profile.uc

import com.pajato.argus.profile.core.Profile
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class AddUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val url = loader.getResource("files/profiles.txt") ?: fail("Could not load profiles resource file!")
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestProfileRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When adding an item, verify behavior`() {
        val item = SelectableProfile(false, false, Profile(0))
        TestProfileRepo.cache.clear()
        ProfileUseCases.add(TestProfileRepo, item)
        assertEquals(1, TestProfileRepo.cache.size)
    }
}
