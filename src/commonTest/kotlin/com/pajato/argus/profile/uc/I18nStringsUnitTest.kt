package com.pajato.argus.profile.uc

import com.pajato.argus.profile.core.I18nStrings
import com.pajato.argus.profile.core.I18nStrings.PROFILE_NOT_A_FILE
import com.pajato.argus.profile.core.I18nStrings.PROFILE_URI_ERROR
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class I18nStringsUnitTest : ReportingTestProfiler() {

    @BeforeTest fun setUp() {
        StringsResource.cache.clear()
        I18nStrings.registerStrings()
    }

    @Test fun `When accessing the localize network strings, verify behavior`() {
        val noUriMessage = "No URI has been injected! A valid file URI is required."
        val invalidPathMessage = "The given file with name 'z.txt' is not a valid file!"
        assertEquals(noUriMessage, get(PROFILE_URI_ERROR))
        assertEquals(invalidPathMessage, get(PROFILE_NOT_A_FILE, Arg("name", "z.txt")))
    }
}
